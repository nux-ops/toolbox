FROM alpine

# install curl git openssh-client nmap tcpdump iperf bind-tools socat openssl
RUN apk update; apk add  curl git openssh-client nmap tcpdump iperf bind-tools socat openssl bash

#install kubectl
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl
#install kustomize
RUN curl -s  https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh | sed -e s/windows/linux/g | bash
RUN mv kustomize /usr/local/bin/
